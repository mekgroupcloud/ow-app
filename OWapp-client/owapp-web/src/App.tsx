import React from 'react'
import "./App.css";
// @ts-ignore
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import {Login} from "./components/pages/login/Login";
import Register from "./components/pages/register/Register";
import Home from "./components/pages/home/Home";

interface PropsInterface {
}

interface StateInterface {
    expanded: boolean,
    fireLink: string,
    alert: any
}

class App extends React.Component<PropsInterface, StateInterface> {
    private static ll: string = "";

    constructor({props}: { props: any }) {
        super(props);
        this.state = {
            expanded: true,
            fireLink: "",
            alert: null
        }
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Login}/>
                    {/*<Route path='/dashboard' component={Dashboard}/>*/}
                    <Route path='/home' component={Home}/>
                    {/*<Route path='/users' component={Users}/>*/}
                    <Route path='/login' component={Login}/>
                    <Route path='/register' component={Register}/>
                    {/*<Route path='/logout' component={Login}/>*/}
                    <Redirect from="*" to="/"/>
                </Switch>
            </BrowserRouter>
        )
    }
};
export default App;