import React, {CSSProperties} from "react";
import {Slider, withStyles} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';


interface PropsInterface {

}

interface StateInterface {

}

export default class ChartContainer extends React.Component<PropsInterface, StateInterface> {
    constructor(props: PropsInterface) {
        super(props);
        this.state = {}
    }


    valuetext(value: number) {
        return `${value}°C`;
    }

    render() {
        const marks = [
            {
                value: 0,
                label: '0°C',
            },
            {
                value: 20,
                label: '20°C',
            },
            {
                value: 37,
                label: '37°C',
            },
            {
                value: 100,
                label: '100°C',
            },
        ];
        return (
            <div>
                <React.Fragment>
                    <Typography id="vertical-slider" gutterBottom>
                        Wykres
                    </Typography>
                    <div style={root}>
                        <PrettoSlider
                            // disabled
                            // orientation="vertical"
                            // aria-labelledby="vertical-slider"
                            valueLabelDisplay="auto"
                            aria-label="pretto slider"
                            defaultValue={20}/>

                    </div>
                </React.Fragment>
            </div>
        )
    }
}

const root: CSSProperties = {
    height: 300,
}
const PrettoSlider = withStyles({
    root: {
        color: '#013A82',
        height: 8,
    },
    thumb: {
        height: 24,
        width: 24,
        backgroundColor: '#FFFFFF',
        border: '2px solid currentColor',
        marginTop: -8,
        marginLeft: -12,
        '&:focus,&:hover,&$active': {
            boxShadow: 'inherit',
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 4px)',
    },
    track: {
        height: 8,
        borderRadius: 4,
    },
    rail: {
        height: 8,
        borderRadius: 4,
    },
})(Slider);

const iOSBoxShadow =
    '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const IOSSlider = withStyles({
    root: {
        color: '#3880ff',
        height: 2,
        padding: '15px 0',
    },
    thumb: {
        height: 28,
        width: 28,
        backgroundColor: '#fff',
        boxShadow: iOSBoxShadow,
        marginTop: -14,
        marginLeft: -14,
        '&:focus,&:hover,&$active': {
            boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
            // Reset on touch devices, it doesn't add specificity
            '@media (hover: none)': {
                boxShadow: iOSBoxShadow,
            },
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 11px)',
        top: -22,
        '& *': {
            background: 'transparent',
            color: '#000',
        },
    },
    track: {
        height: 2,
    },
    rail: {
        height: 2,
        opacity: 0.5,
        backgroundColor: '#bfbfbf',
    },
    mark: {
        backgroundColor: '#bfbfbf',
        height: 8,
        width: 1,
        marginTop: -3,
    },
    markActive: {
        opacity: 1,
        backgroundColor: 'currentColor',
    },
})(Slider);