import React, {CSSProperties} from "react";

const divContainer: CSSProperties = {
    padding: "4px",
    borderRadius: "10px",
    textAlign: "center",
    border: "1px solid rgba(27,76,130,0.5)",
    height: "100%",
    alignContent: "center",
    boxShadow: "0px 3px 6px #00000029"
};
const divContainerNoBorder: CSSProperties = {
    borderRadius: "10px",
    textAlign: "center",
    height: "100%",
    alignContent: "center",
    backgroundColor: "rgba(255,255,255,0.25)"
};

export default function PaperContanier(props: any) {
    return (
        <div style={divContainer}>
            <div style={divContainerNoBorder}>
                {props.children}
            </div>
        </div>
    );

}