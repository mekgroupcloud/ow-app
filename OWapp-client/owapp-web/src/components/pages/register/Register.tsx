import React from "react";

import {Container, FormGroup, Grid, TextField} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Jumbotron} from "reactstrap";

interface PropsInterface {
    register(user: any): void,

    registering: boolean | false

}

interface StateInterface {
    user: {
        firstName: string | '',
        lastName: string | '',
        username: string | '',
        password: string | ''
    },
    submitted: boolean | false
}

export default class Register extends React.Component<PropsInterface, StateInterface> {

    constructor(props: any) {
        super(props);

        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                password: ''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    handleChange(event: any) {
        const {name, value} = event.target;
        const {user} = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event: any) {
        event.preventDefault();

        this.setState({submitted: true});
        const {user} = this.state;
        if (user.firstName && user.lastName && user.username && user.password) {
            this.props.register(user);
        }
    }


    render() {
        const {registering} = this.props;
        const {user, submitted} = this.state;

        return (
            <Jumbotron style={{backgroundColor: "#DFE5EA", height: "100vh"}}>
                <Container className="fluid">
                    <h1 style={{color: "#0064ac", fontSize: 45}}>OWapp</h1>
                    <Grid container spacing={5}>
                        <Grid item xl={3} md={3} sm={1} xs={1}/>
                        <Grid item xl={6} md={6} sm={10} xs={10}>
                            <Grid item xl={12} md={12} xs={12} style={{paddingLeft: 20}}>
                                <h2 style={{color: "#0064ac", marginBottom: 50}}>Rejestracja</h2>
                                <form name="form" onSubmit={this.handleSubmit}>
                                    <FormGroup
                                        className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
                                        <TextField variant="outlined"
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   type="text" name="firstName" value={user.firstName} label={"IMIĘ"}
                                                   onChange={this.handleChange}/>
                                        {submitted && !user.firstName &&
                                        <div className="help-block">Imię</div>
                                        }
                                    </FormGroup>
                                    <FormGroup
                                        className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                                        <TextField variant="outlined"
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   type="text" name="lastName" value={user.lastName} label={"NAZWISKO"}
                                                   onChange={this.handleChange}/>
                                        {submitted && !user.lastName &&
                                        <div className="help-block">Nazwisko jest wymagane</div>
                                        }
                                    </FormGroup>
                                    <FormGroup
                                        className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                                        <TextField variant="outlined"
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   type="email" name="username" value={user.username} label={"EMAIL"}
                                                   onChange={this.handleChange}/>
                                        {submitted && !user.username &&
                                        <div className="help-block">Email jest wymagany</div>
                                        }
                                    </FormGroup>
                                    <FormGroup
                                        className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                                        <TextField variant="outlined"
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   type="password" name="password" label={"HASŁO"}
                                                   value={user.password} onChange={this.handleChange}/>
                                        {submitted && !user.password &&
                                        <div className="help-block">Hasło jest wymagane</div>
                                        }
                                    </FormGroup>
                                    <div className="form-group">
                                        <button className="btn btn-primary pull-right"
                                                style={{width: 150}}>Zarejestruj
                                        </button>
                                        {registering &&
                                        <img
                                            src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>
                                        }
                                        <Link to="/login" className="btn btn-link">Anuluj</Link>
                                    </div>
                                </form>
                            </Grid>
                        </Grid>
                    </Grid>


                    {/*<Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>*/}
                    {/*    <Form.Row>*/}
                    {/*        <Form.Group as={Col} md="6" controlId="validationCustom01">*/}
                    {/*            <Form.Label>Imię</Form.Label>*/}
                    {/*            <Form.Control*/}
                    {/*                required*/}
                    {/*                type="text"*/}
                    {/*                placeholder="Wprowadź imię"*/}
                    {/*                defaultValue=""*/}
                    {/*            />*/}
                    {/*            <Form.Control.Feedback>Ok!</Form.Control.Feedback>*/}
                    {/*        </Form.Group>*/}
                    {/*        <Form.Group as={Col} md="6" controlId="validationCustom02">*/}
                    {/*            <Form.Label>Nazwisko</Form.Label>*/}
                    {/*            <Form.Control*/}
                    {/*                required*/}
                    {/*                type="text"*/}
                    {/*                placeholder="Wprowadź nazwisko"*/}
                    {/*                defaultValue=""*/}
                    {/*            />*/}
                    {/*            <Form.Control.Feedback>Ok!</Form.Control.Feedback>*/}
                    {/*        </Form.Group>*/}
                    {/*    </Form.Row>*/}
                    {/*    <Form.Group controlId="validationCustomEmail">*/}
                    {/*        <Form.Label>Email</Form.Label>*/}
                    {/*        <InputGroup>*/}
                    {/*            <Form.Control*/}
                    {/*                type="email"*/}
                    {/*                placeholder="Podaj email"*/}
                    {/*                required*/}
                    {/*            />*/}
                    {/*            <Form.Control.Feedback type="invalid">*/}
                    {/*                Wprowadź poprawny adres email.*/}
                    {/*            </Form.Control.Feedback>*/}
                    {/*        </InputGroup>*/}
                    {/*    </Form.Group>*/}
                    {/*    <Form.Group controlId="validationPassword">*/}
                    {/*        <Form.Label>Hasło</Form.Label>*/}
                    {/*        <Form.Control type="password" name="password" placeholder="Podaj hasło" required onChange={this.handleInputChage}/>*/}
                    {/*        <Form.Control.Feedback type="invalid">*/}
                    {/*            Hasła różnią się.*/}
                    {/*        </Form.Control.Feedback>*/}
                    {/*    </Form.Group>*/}
                    {/*    <Form.Group controlId="validationPassword">*/}
                    {/*        <Form.Control type="password" name="passwordConfirm" placeholder="Powtórz hasło" required onChange={this.handleInputChage}/>*/}
                    {/*        <Form.Control.Feedback type="invalid">*/}
                    {/*            Hasła różnią się.*/}
                    {/*        </Form.Control.Feedback>*/}
                    {/*    </Form.Group>*/}
                    {/*    <Form.Group>*/}
                    {/*        <Form.Check*/}
                    {/*            required*/}
                    {/*            label="Agree to terms and conditions"*/}
                    {/*            feedback="You must agree before submitting."*/}
                    {/*        />*/}
                    {/*    </Form.Group>*/}
                    {/*    <Button type="submit">Submit form</Button>*/}
                    {/*</Form>*/}
                </Container>
            </Jumbotron>
        )
    }

}
