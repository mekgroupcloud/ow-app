import React from "react";
import {Container, FormGroup, FormHelperText, Grid, TextField} from "@material-ui/core";
import {Link} from 'react-router-dom';
import {Jumbotron} from "reactstrap";

interface PropsInterface {
    login(username: string, password: string): void,

    loggingIn: boolean
}

interface StateInterface {
    username: string | '',
    password: string | '',
    submitted: boolean | false
}

export class Login extends React.Component<PropsInterface, StateInterface> {
    constructor(props: any) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e: any) {
        const {name, value} = e.target;
        // @ts-ignore
        this.setState({[name]: value});
    }

    handleSubmit(e: any) {
        e.preventDefault();


        this.setState({submitted: true});
        const {username, password} = this.state;
        if (username && password) {
            console.log(username + " " + password)
            // this.props.login(username, password);
            fetch('localhost:8080/login', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                    Authorization: 'TheReturnedToken',
                })
            }) /*end fetch */
                .then(results => results.json())
                .then(data => console.log(data)
                )
        }
    }

    render() {
        const {loggingIn} = this.props;
        const {username, password, submitted} = this.state;
        return (
            <Jumbotron style={{backgroundColor: "#DFE5EA", height: "100vh"}}>
                <Container className="fluid">
                    <h1 style={{color: "#0064ac", fontSize: 45}}>OWapp</h1>
                    <Grid container spacing={5}>
                        <Grid item xl={3} md={3} sm={1} xs={1}/>
                        <Grid item xl={6} md={6} sm={10} xs={10}>
                            <Grid item xl={12} md={12} xs={12} style={{paddingLeft: 20}}>
                                <h2 style={{color: "#0064ac", marginBottom: 50}}>Logowanie </h2>

                                <form name="form" onSubmit={this.handleSubmit}>

                                    <FormGroup className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                                        <TextField variant="outlined" type="text" name="username" value={username}
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   label="LOGIN/EMAIL"
                                                   onChange={this.handleChange}/>
                                        {submitted && !username &&
                                        <FormHelperText error={true}>LOGIN/EMAIL jest wymagany</FormHelperText>
                                        }

                                    </FormGroup>

                                    <FormGroup className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                                        <TextField variant="outlined"
                                                   style={{backgroundColor: "#fff", borderRadius: "5px"}}
                                                   type="password" name="password"
                                                   label="HASŁO"
                                                   value={password}
                                                   onChange={this.handleChange}/>

                                        {submitted && !password &&
                                        <FormHelperText error={true}>HASŁO jest wymagane</FormHelperText>
                                        }
                                    </FormGroup>

                                    <div className="form-group">
                                        {loggingIn &&
                                        <img
                                            src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>
                                        }
                                        <Link to="/register">Rejestracja</Link>
                                        <button className="btn btn-primary pull-right" style={{width: 150}}>Zaloguj
                                        </button>
                                    </div>
                                </form>
                            </Grid>
                        </Grid>
                    </Grid>


                    {/*<Paper className={classes.padding}>*/}
                    {/*    <div className={classes.margin}>*/}
                    {/*        <Grid container spacing={8} alignItems="flex-end">*/}
                    {/*            <Grid item>*/}
                    {/*                <Face/>*/}
                    {/*            </Grid>*/}
                    {/*            <Grid item md={true} sm={true} xs={true}>*/}
                    {/*                <TextField id="username" label="Username" type="email" fullWidth autoFocus required/>*/}
                    {/*            </Grid>*/}
                    {/*        </Grid>*/}
                    {/*        <Grid container spacing={8} alignItems="flex-end">*/}
                    {/*            <Grid item>*/}
                    {/*                <Fingerprint/>*/}
                    {/*            </Grid>*/}
                    {/*            <Grid item md={true} sm={true} xs={true}>*/}
                    {/*                <TextField id="username" label="Password" type="password" fullWidth required/>*/}
                    {/*            </Grid>*/}
                    {/*        </Grid>*/}
                    {/*        <Grid container alignItems="center" justify="space-between">*/}
                    {/*            <Grid item>*/}
                    {/*                <FormControlLabel control={*/}
                    {/*                    <Checkbox*/}
                    {/*                        color="primary"*/}
                    {/*                    />*/}
                    {/*                } label="Remember me"/>*/}
                    {/*            </Grid>*/}
                    {/*            <Grid item>*/}
                    {/*                <Button disableFocusRipple disableRipple style={{textTransform: "none"}} variant="text"*/}
                    {/*                        color="primary">Forgot password ?</Button>*/}
                    {/*            </Grid>*/}
                    {/*        </Grid>*/}
                    {/*        <Grid container justify="center" style={{marginTop: '10px'}}>*/}
                    {/*            <Button variant="outlined" color="primary" style={{textTransform: "none"}}>Login</Button>*/}
                    {/*        </Grid>*/}
                    {/*    </div>*/}
                    {/*</Paper>*/}
                </Container>
            </Jumbotron>
        );
    }
}
