import React, {CSSProperties} from "react";
import {Row} from "reactstrap";
import Chart from "react-google-charts";
import InfoPanel from "./panels/InfoPanel";
import {CircularProgress, Grid} from "@material-ui/core";
import PaperContainer from "./../../containers/PaperContainer";
import ChartContainer from "../../containers/ChartContainer";

const divContainer: CSSProperties = {
    padding: "4px",
    borderRadius: "10px",
    textAlign: "center",
    border: "1px solid rgba(27,76,130,0.5)",
    height: "100%",
    alignContent: "center",
    boxShadow: "0px 3px 6px #00000029"
};
const divContainerNoBorder: CSSProperties = {
    borderRadius: "10px",
    textAlign: "center",
    height: "100%",
    alignContent: "center",
    backgroundColor: "rgba(255,255,255,0.25)"
};
const divTitle: CSSProperties = {
    color: "#fff",
    borderRadius: "10px",
    backgroundColor: "#1B4C82",
    fontSize: "14px",
    padding: "5px",
    margin: "5px",
    textTransform: "uppercase",
    fontWeight: "bold"
};
const rowTitle: CSSProperties = {marginTop: "40px", marginBottom: "10px"};


interface PropsInterface {
    expanded: boolean
}

interface StateInterface {
}

export default class Dashboard extends React.Component<PropsInterface, StateInterface> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div className={"fluid"}>
                <div style={{
                    position: "relative",
                    overflow: "hidden",
                    transition: "all .15s",
                    padding: "0 20px",
                    marginLeft: this.props.expanded ? "260px" : "80px"
                }}>
                    <h1 style={{color: "#184386", marginBottom: 50}}>Dashboard</h1>
                    <Row style={rowTitle}>
                        <div style={divTitle}>
                            Wynik
                        </div>
                    </Row>

                    <Grid container spacing={2}>
                        <Grid item xl={3} lg={3} md={3} sm={3} xs={3}>
                            <PaperContainer>
                                <InfoPanel title={"Dzisiejsza sprzedaż"} value={"2 040,00"}/>
                            </PaperContainer>
                        </Grid>
                        <Grid item xl={3} lg={3} md={3} sm={3} xs={3}>
                            <PaperContainer>
                                <InfoPanel title={"Łącznie prowizja do wypłaty"} value={"40,80"}/>
                            </PaperContainer>
                        </Grid>
                    </Grid>

                    <Row style={rowTitle}>
                        <div style={divTitle}>
                            Podsumowanie sprzedaży z okresu
                        </div>
                    </Row>

                    <Grid container spacing={2}>
                        <Grid item xl={3} lg={3} md={4} sm={12} xs={12}>
                            <div style={{
                                padding: "4px",
                                borderRadius: "10px",
                                textAlign: "center",
                                border: "1px solid rgba(27,76,130,0.5)",
                                height: "100%",
                                alignContent: "center",
                                boxShadow: "0px 3px 6px #00000029"
                            }}>
                                <Grid container spacing={1}>
                                    <Grid item xl={12} md={12} xs={12}>
                                        <InfoPanel title={"Ostatnie 7 dni"} value={"8 024,00"}/>
                                    </Grid>
                                    <Grid item xl={12} md={12} xs={12}>
                                        <InfoPanel title={"Ostatnie 14 dni"} value={"48 024,00"}/>
                                    </Grid>
                                    <Grid item xl={12} md={12} xs={12}>
                                        <InfoPanel title={"Ostatnie 30 dni"} value={"92 032,00"}/>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                        <Grid item xl={4} lg={4} md={8} sm={12} xs={12}>
                            <PaperContainer>
                                <Chart
                                    width={"100%"}
                                    height={"100%"}
                                    chartType="LineChart"
                                    loader={<CircularProgress/>}
                                    data={[
                                        ['x', 'PLN'],
                                        [0, 2],
                                        [1, 10],
                                        [2, 23],
                                            [3, 17],
                                            [4, 18],
                                            [5, 9],
                                            [6, 11],
                                            [7, 27]
                                        ]}
                                        options={{
                                            title: "",
                                            hAxis: {
                                                title: 'Dzień',
                                                minValue: 0,
                                                maxValue: 7
                                            },
                                            vAxis: {
                                                title: 'Sprzedaż',
                                            },
                                            backgroundColor: "transparent",
                                            legend: {position: 'top', alignment: 'end'}
                                        }}
                                    legendToggle={true}
                                    rootProps={{'data-testid': '1'}}
                                />
                            </PaperContainer>
                        </Grid>
                        <Grid item xl={5} lg={5} md={12} sm={12} xs={12}>
                            <PaperContainer>
                                <Chart
                                    width={"100%"}
                                    height={"100%"}
                                    chartType="LineChart"
                                    loader={<CircularProgress/>}
                                    data={[
                                        ['x', 'PLN'],
                                        [0, 2],
                                        [1, 10],
                                        [2, 23],
                                            [3, 17],
                                            [4, 18],
                                            [5, 9],
                                            [6, 11],
                                            [7, 27],
                                            [8, 2],
                                            [9, 10],
                                            [10, 23],
                                            [11, 17],
                                            [12, 18],
                                            [13, 9],
                                            [14, 11],
                                        ]}
                                        options={{
                                            title: "",
                                            hAxis: {
                                                title: 'Dzień',
                                                minValue: 0,
                                                maxValue: 14
                                            },
                                            vAxis: {
                                                title: 'Sprzedaż',
                                            },
                                            backgroundColor: "transparent",
                                            legend: {position: 'top', alignment: 'end'}
                                        }}
                                    legendToggle={true}
                                    rootProps={{'data-testid': '1'}}
                                />
                            </PaperContainer>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                            <PaperContainer>
                                <Chart
                                    width={"100%"}
                                    height={"100%"}
                                    chartType="LineChart"
                                    loader={<CircularProgress/>}
                                    data={[
                                        ['x', 'PLN'],
                                        [0, 2],
                                        [1, 10],
                                        [2, 23],
                                            [3, 17],
                                            [4, 18],
                                            [5, 9],
                                            [6, 11],
                                            [7, 27],
                                            [8, 2],
                                            [9, 10],
                                            [10, 23],
                                            [11, 17],
                                            [12, 18],
                                            [13, 9],
                                            [14, 11],
                                            [15, 2],
                                            [16, 10],
                                            [17, 23],
                                            [18, 17],
                                            [19, 18],
                                            [20, 9],
                                            [21, 11],
                                            [22, 27],
                                            [23, 2],
                                            [24, 10],
                                            [25, 23],
                                            [26, 17],
                                            [27, 18],
                                            [28, 9],
                                            [29, 11],
                                            [30, 11]
                                        ]}
                                        options={{
                                            title: "",
                                            hAxis: {
                                                title: 'Dzień',
                                                minValue: 0,
                                                maxValue: 15
                                            },
                                            vAxis: {
                                                title: 'Sprzedaż',
                                            },
                                            backgroundColor: "transparent",
                                            legend: {position: 'top', alignment: 'end'}
                                        }}
                                    legendToggle={true}
                                    rootProps={{'data-testid': '1'}}
                                />
                            </PaperContainer>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} style={{height: "300px"}}>
                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                            <ChartContainer/>
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }
}
