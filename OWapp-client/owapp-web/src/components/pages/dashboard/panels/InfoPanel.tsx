import React, {CSSProperties} from "react";
import {Container} from "@material-ui/core";

const contStyle: CSSProperties = {
    borderRadius: "10px",
    textAlign: "center",
    backgroundColor: "rgba(255,255,255,0.5)",
    height: "100%",
    padding: "0px"
};
const divTitleStyle: CSSProperties = {paddingBottom: "10px", color: "#0064ac", fontWeight: "bold"};
const divValueStyle: CSSProperties = {
    borderRadius: "0px 10px 0px 10px",
    backgroundColor: "#8F4737",
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: "1.2em",
    width: "75%"
};

interface PropsInterface {
    title: string,
    value: string
}

interface StateInterface {

}

export default class InfoPanel extends React.Component<PropsInterface, StateInterface> {
    constructor(props: any) {
        super(props);
    }

    render() {
        const {title, value} = this.props;
        return (
            <Container style={contStyle}>
                <div style={divTitleStyle}>{title}</div>
                <div style={divValueStyle}>
                    {value} zł
                </div>
            </Container>
        );
    }
}