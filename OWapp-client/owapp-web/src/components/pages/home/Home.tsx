import React, {CSSProperties} from "react";
// @ts-ignore
import SideNav, {NavIcon, NavItem, NavText} from '@trendmicro/react-sidenav';
// @ts-ignore
import {BrowserRouter, Route} from "react-router-dom";
import Dashboard from "../dashboard/Dashboard";


const divContainer: CSSProperties = {
    padding: "4px",
    borderRadius: "10px",
    textAlign: "center",
    border: "1px solid rgba(27,76,130,0.5)",
    height: "100%",
    alignContent: "center",
    boxShadow: "0px 3px 6px #00000029"
};
const divContainerNoBorder: CSSProperties = {
    borderRadius: "10px",
    textAlign: "center",
    height: "100%",
    alignContent: "center",
    backgroundColor: "rgba(255,255,255,0.25)"
};
const divTitle: CSSProperties = {
    color: "#fff",
    borderRadius: "10px",
    backgroundColor: "#1B4C82",
    fontSize: "14px",
    padding: "5px",
    margin: "5px",
    textTransform: "uppercase",
    fontWeight: "bold"
};
const rowTitle: CSSProperties = {marginTop: "40px", marginBottom: "10px"};


interface PropsInterface {

}

interface StateInterface {
    expanded: boolean,
}

export default class Home extends React.Component<PropsInterface, StateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            expanded: false,
        };
    }

    render() {

        return (
            <div className={"fluid"} style={{background: "#DFE5EA"}} onClick={() => {
                this.state.expanded ? this.setState({expanded: false}) : console.log("")
            }}>
                <BrowserRouter>
                    <Route render={({location, history}) => <React.Fragment>
                        <SideNav expanded={this.state.expanded} onSelect={(selected: string) => {
                            const to = '/' + selected;
                            if (location.pathname !== to) {
                                history.push(to);
                            }
                        }}
                                 onToggle={(expanded: boolean) => {
                                     this.setState({expanded: expanded})
                                 }}
                                 style={{background: "#184386"}}>
                            <SideNav.Toggle/>
                            <SideNav.Nav defaultSelected="dashboard">
                                <NavItem eventKey="dashboard">
                                    <NavIcon>
                                        <i className={"fa fa-fw fa-dashboard"} style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Dashboard
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="users">
                                    <NavIcon>
                                        <i className="fa fa-fw fa-user" style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Users
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="messages">
                                    <NavIcon>
                                        <i className="fa fa-fw fa-envelope-o" style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Users
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="login">
                                    <NavIcon>
                                        <i className="fa fa-fw fa-sign-in" style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Login
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="logout">
                                    <NavIcon>
                                        <i className="fa fa-fw fa-sign-out" style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Users
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="register">
                                    <NavIcon>
                                        <i className="fa fa-fw fa-user-plus" style={{fontSize: '1.75em'}}/>
                                    </NavIcon>
                                    <NavText>
                                        Register
                                    </NavText>
                                </NavItem>
                            </SideNav.Nav>
                        </SideNav>

                        {/*<div style={{*/}
                        {/*    position: "relative",*/}
                        {/*    overflow: "hidden",*/}
                        {/*    transition: "all .15s",*/}
                        {/*    padding: "0 20px",*/}
                        {/*    marginLeft: this.state.expanded? "260px" : "80px",*/}
                        {/*    border:"1px solid black",*/}
                        {/*    height:"100vh"*/}
                        {/*}}>*/}

                        {/*    <Route exact path='/' component={Dashboard}/>*/}
                        {/*    <Route path='/dashboard' component={Dashboard}/>*/}
                        {/*</div>*/}

                        {/*<Container style={{height: "100vh", zIndex: 99999, backgroundColor:"rgba(1,88,164,0.3)"}}>*/}
                        {/*    <Row>*/}
                        {/*        <Col lg={{size: 9, offset: 1}} xl={{size: 9, offset: 1}} md={{size: 11, offset: 1}} sm={{size: 11, offset: 1}}*/}
                        {/*             xs={{size: 11, offset: 1}}>*/}
                        {/*            <Route exact path='/' component={Dashboard}/>*/}
                        {/*            <Route path='/dashboard' component={Dashboard}/>*/}
                        {/*        </Col>*/}
                        {/*    </Row>*/}
                        {/*</Container>*/}
                    </React.Fragment>}/>

                    <div style={{
                        position: "relative",
                        overflow: "hidden",
                        transition: "all .15s",
                        padding: "0 20px",
                        marginLeft: this.state.expanded ? "260px" : "80px",
                        height: "100vh"
                    }}>

                        <Route exact path='/home' component={Dashboard}/>
                        <Route path='/dashboard' component={Dashboard}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}
